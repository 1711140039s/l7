class Product < ApplicationRecord
  belongs_to :cart
  has_many :cart_items
  has_many :cart_products, through: :cart_items, source: :cart
    
   # def take(cart)
     #   cart_item.create(cart_id: cart.id)
   # end
    
end
