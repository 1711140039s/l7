class Cart < ApplicationRecord
     has_many :products
     has_many :cart_items
     has_many :cart_products, through: :cart_items, source: :product
end
