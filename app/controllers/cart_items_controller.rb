class CartItemsController < ApplicationController
    
    def create
     @products = Product.all
     product = Product.find(params[:product_id])
     cart = current_cart
     cart.cart_products << product
     redirect_to '/'
    end
    
    
    def destroy
     @carts = Cart.all
     cart = Cart.find(params[:product_id])
     product = Product.find_by(name: session[:cart_id])
     cart.cart_items.find_by(product_id: product.id).destroy
     redirect_to '/'
    end
end
